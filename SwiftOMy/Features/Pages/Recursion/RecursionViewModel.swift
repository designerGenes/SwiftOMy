//
//  RecursionViewModel.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/11/23.
//

import Foundation
import SwiftUI
import Combine

enum RecursionCase: String {
    case factorial = "Factorial"
    case binarySearch = "Binary Search"
    case towerOfHanoi = "Tower of Hanoi"
    case treeTraversal = "Tree Traversal"
    static var allValues: [RecursionCase] {
        [.factorial, .binarySearch, .towerOfHanoi, .treeTraversal]
    }
    
    func inChallengeForm() -> String {
        switch self {
        case .factorial:
            return """
            Implement a function that produces the factorial of a input number
            """
        case .binarySearch:
            return """
            Implement binary search
            """
        case .towerOfHanoi:
            return """
             The Tower of Hanoi puzzle consists of three towers (A, B, and C) and a number of disks of different sizes. The objective is to move all the disks from the source tower to the destination tower, while following these rules:
            
              (1) Only one disk can be moved at a time,
            
              (2) A larger disk cannot be placed on top of a smaller disk.
            
            The task is to write a recursive function that prints the sequence of moves required to solve the puzzle for a given number of disks.
            """
        case .treeTraversal:
            return """
            A tree traversal challenge implements recursive functions for different tree traversal methods: **in-order**, **pre-order**, and **post-order** traversal. Given a binary tree, the task is to write three separate recursive functions to traverse the tree and print the values of its nodes in a specific order.
            - In-order traversal visits the left subtree, then the current node, and finally the right subtree.
            - Pre-order traversal visits the current node, then the left subtree, and finally the right subtree.
            - Post-order traversal visits the left subtree, then the right subtree, and finally the current node.
            """
        }
    }
}



enum RecursionFrame {
    case calledSelf(fnName: String, inputVals: [String: Any])
    case foundNothing
    case foundValue(value: Any)
    
    func explain() -> String {
        switch self {
        case .calledSelf(let fnName, let inputVals):
            return "\(fnName) recursed with input values \(inputVals.map {"\($0.0): \($0.1)"}.joined(separator: ", "))"
        case .foundNothing:
            return "found no matching value"
        case .foundValue(let value):
            return "found the value \(value)"
        }
    }
}

class RecursionViewModel: ObservableObject {
    @Published var selectedRecursionCaseIdx = 0
    

    init(selectedRecursionCaseIdx: Int = 0) {
        self.selectedRecursionCaseIdx = selectedRecursionCaseIdx
        
    }
}

