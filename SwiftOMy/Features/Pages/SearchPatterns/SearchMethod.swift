//
//  SearchMethod.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/12/23.
//

import Foundation
enum SearchAlgorithm: String {
    case rabinKarp = "Rabin-Karpe"
    case knuthMorrisPratt = "Knuth-Morris-Pratt"
    case boyerMoore = "Boyer-Moore"
    case Z = "Z"
    static var allCases: [SearchAlgorithm] {
        return [SearchAlgorithm.rabinKarp, .knuthMorrisPratt, .boyerMoore, .Z]
    }

    func explanation() -> String {
        switch self {
        case .rabinKarp:
            return "The Rabin-Karp algorithm is a string searching algorithm that uses hashing to find any one of a set of pattern strings in a text. It uses a rolling hash to quickly hash the pattern and segments of the text. When the hash values match, a direct comparison is made to confirm the match. This algorithm is particularly effective when searching for multiple patterns in a text."
        case .knuthMorrisPratt:
            return "The KMP algorithm is used for pattern searching within a text. It preprocesses the pattern and constructs an auxiliary array of the longest proper prefix that is also a proper suffix. The array is used to skip characters while matching, leading to a linear time complexity. When a mismatch occurs, instead of moving the pattern by one character to the right, the next character of the text aligns with the last occurrence of the mismatched character in the pattern."
        case .boyerMoore:
            return "The Boyer-Moore algorithm is an efficient string searching algorithm that combines information about the occurrence of individual characters in the pattern to skip sections of the text, thus making it faster than brute force algorithms. It preprocesses the pattern into two tables: a bad character heuristic table and a good suffix heuristic table. These tables are then used to skip characters while searching."
            
        case .Z:
            return "The Z algorithm finds all occurrences of a pattern in a text in linear time. It constructs an auxiliary array, Z array, that stores the length of the substring which is also a prefix of the main string. Each index in the Z array corresponds to each character in the string. For example, Z[i] is equal to the greatest number of characters starting from the character indexed i that matches the characters starting from the beginning of the string. This helps in avoiding unnecessary comparisons."
        }
    }
    
    func explanationCode() -> String {
        switch self {
        case .boyerMoore:
            return
"""
    func boyerMoore(_ pattern: String, _ text: String) -> [Int] {
        let patternLength = pattern.count
        let textLength = text.count
        guard patternLength > 0 && patternLength <= textLength else { return [] }
        var result: [Int] = []
        let pattern = Array(pattern)
        let text = Array(text)
        var badMatchTable: [Character: Int] = [:]
        for (i, char) in pattern.enumerated() {
            badMatchTable[char] = max(1, patternLength - i - 1)
        }
        var offset = 0
        while offset <= textLength - patternLength {
            var skip = 0
            for (i, char) in pattern.enumerated().reversed() {
                if char != text[offset + i] {
                    skip = badMatchTable[text[offset + i]] ?? patternLength
                    break
                }
            }
            if skip == 0 {
                result.append(offset)
                skip = patternLength
            }
            offset += skip
        }
        return result
    }
"""
        case .rabinKarp:
            return
"""
    func rabinKarp(_ pattern: String, _ text: String) -> [Int] {
        let patternLength = pattern.count
        let textLength = text.count
        guard patternLength > 0 && patternLength <= textLength else { return [] }
        var result: [Int] = []
        let pattern = Array(pattern)
        let text = Array(text)
        let patternHash = hash(pattern)
        var textHash = hash(Array(text[0..<patternLength]))
        for i in 0...textLength - patternLength {
            if patternHash == textHash {
                if pattern == Array(text[i..<i + patternLength]) {
                    result.append(i)
                }
            }
            if i < textLength - patternLength {
                textHash = rollingHash(textHash, text[i], text[i + patternLength], patternLength)
            }
        }
        return result
    }

    func hash(_ string: [Character]) -> Int {
        var hash = 0
        for char in string {
            hash = (hash * 31 + Int(char.asciiValue!)) % 1000000007
        }
        return hash
    }

    func rollingHash(_ hash: Int, _ dropped: Character, _ added: Character, _ patternLength: Int) -> Int {
        let prime = 1000000007
        var droppedEffect = Int(dropped.asciiValue!) * Int(pow(31.0, Double(patternLength - 1))) % prime
        var newHash = (hash - droppedEffect + prime) % prime
        newHash = (newHash * 31 + Int(added.asciiValue!)) % prime
        return newHash
    }

    
"""
        case .knuthMorrisPratt:
            return
"""
    func knuthMorrisPratt(_ pattern: String, _ text: String) -> [Int] {
        let patternLength = pattern.count
        let textLength = text.count
        guard patternLength > 0 && patternLength <= textLength else { return [] }
        var result: [Int] = []
        let pattern = Array(pattern)
        let text = Array(text)
        let lps = computeLPS(pattern)
        var i = 0
        var j = 0
        while i < textLength {
            if pattern[j] == text[i] {
                i += 1
                j += 1
            }
            if j == patternLength {
                result.append(i - j)
                j = lps[j - 1]
            } else if i < textLength && pattern[j] != text[i] {
                if j != 0 {
                    j = lps[j - 1]
                } else {
                    i += 1
                }
            }
        }
        return result
    }

    func computeLPS(_ pattern: [Character]) -> [Int] {
        var lps: [Int] = Array(repeating: 0, count: pattern.count)
        var len = 0
        var i = 1
        while i < pattern.count {
            if pattern[i] == pattern[len] {
                len += 1
                lps[i] = len
                i += 1
            } else {
                if len != 0 {
                    len = lps[len - 1]
                } else {
                    lps[i] = 0
                    i += 1
                }
            }
        }
        return lps
    }
"""
        case .Z:
            return
"""
    func calculateZArray(string: String) -> [Int] {
        let str = Array(string)
        var Z = [Int](repeating: 0, count: str.count)
        var left = 0
        var right = 0
        for k in 1..<str.count {
            if k > right {
                left = k
                right = k
                while right < str.count && str[right-left] == str[right] {
                    right += 1
                }
                Z[k] = right - left
                right -= 1
            } else {
                let k1 = k - left
                if Z[k1] < right - k + 1 {
                    Z[k] = Z[k1]
                } else {
                    left = k
                    while right < str.count && str[right-left] == str[right] {
                        right += 1
                    }
                    Z[k] = right - left
                    right -= 1
                }
            }
        }
        return Z
    }

    func search(text: String, pattern: String) -> [Int] {
        let str = pattern + "$" + text
        let Z = calculateZArray(string: str)
        var result: [Int] = []
        for i in 0..<Z.count {
            if Z[i] == pattern.count {
                result.append(i - pattern.count - 1)
            }
        }
        return result
    }
"""
        }
    }
}

//MARK: - Rabin-Karpe
func rabinKarpSearch(_ pattern: String, _ text: String) -> [Int] {
    func hash(_ string: [Character]) -> Int {
        var hash = 0
        for char in string {
            hash = (hash * 31 + Int(char.asciiValue!)) % 1000000007
        }
        return hash
    }
    
    func rollingHash(_ hash: Int, _ dropped: Character, _ added: Character, _ patternLength: Int) -> Int {
        let prime = 1000000007
        var droppedEffect = Int(dropped.asciiValue!) * Int(pow(31.0, Double(patternLength - 1))) % prime
        var newHash = (hash - droppedEffect + prime) % prime
        newHash = (newHash * 31 + Int(added.asciiValue!)) % prime
        return newHash
    }

    
    let patternLength = pattern.count
    let textLength = text.count
    guard patternLength > 0 && patternLength <= textLength else { return [] }
    var result: [Int] = []
    let pattern = Array(pattern)
    let text: [Character] = Array(text)
    let patternHash = hash(Array(pattern))
    var textHash = hash(Array(text[0..<patternLength]))
    for i in 0...textLength - patternLength {
        if patternHash == textHash {
            if pattern == Array(text[i..<i + patternLength]) {
                result.append(i)
            }
        }
        if i < textLength - patternLength {
            textHash = rollingHash(textHash, text[i], text[i + patternLength], patternLength)
        }
    }
    return result
}

//MARK: - Knuth Morris Pratt
func knuthMorrisPrattSearch(_ pattern: String, _ text: String) -> [Int] {
    let patternLength = pattern.count
    let textLength = text.count
    guard patternLength > 0 && patternLength <= textLength else { return [] }
    var result: [Int] = []
    let pattern = Array(pattern)
    let text = Array(text)
    let lps = computeLPS(pattern)
    var i = 0
    var j = 0
    while i < textLength {
        if pattern[j] == text[i] {
            i += 1
            j += 1
        }
        if j == patternLength {
            result.append(i - j)
            j = lps[j - 1]
        } else if i < textLength && pattern[j] != text[i] {
            if j != 0 {
                j = lps[j - 1]
            } else {
                i += 1
            }
        }
    }
    return result
}

func computeLPS(_ pattern: [Character]) -> [Int] {
    var lps: [Int] = Array(repeating: 0, count: pattern.count)
    var len = 0
    var i = 1
    while i < pattern.count {
        if pattern[i] == pattern[len] {
            len += 1
            lps[i] = len
            i += 1
        } else {
            if len != 0 {
                len = lps[len - 1]
            } else {
                lps[i] = 0
                i += 1
            }
        }
    }
    return lps
}

//MARK: - Boyer Moore
func boyerMooreSearch(_ pattern: String, _ text: String) -> [Int] {
    let patternLength = pattern.count
    let textLength = text.count
    guard patternLength > 0 && patternLength <= textLength else { return [] }
    var result: [Int] = []
    let pattern = Array(pattern)
    let text = Array(text)
    var badMatchTable: [Character: Int] = [:]
    for (i, char) in pattern.enumerated() {
        badMatchTable[char] = max(1, patternLength - i - 1)
    }
    var offset = 0
    while offset <= textLength - patternLength {
        var skip = 0
        for (i, char) in pattern.enumerated().reversed() {
            if char != text[offset + i] {
                skip = badMatchTable[text[offset + i]] ?? patternLength
                break
            }
        }
        if skip == 0 {
            result.append(offset)
            skip = patternLength
        }
        offset += skip
    }
    return result
}

//MARK: - Z
func calculateZArray(string: String) -> [Int] {
    let str = Array(string)
    var Z = [Int](repeating: 0, count: str.count)
    var left = 0
    var right = 0
    for k in 1..<str.count {
        if k > right {
            left = k
            right = k
            while right < str.count && str[right-left] == str[right] {
                right += 1
            }
            Z[k] = right - left
            right -= 1
        } else {
            let k1 = k - left
            if Z[k1] < right - k + 1 {
                Z[k] = Z[k1]
            } else {
                left = k
                while right < str.count && str[right-left] == str[right] {
                    right += 1
                }
                Z[k] = right - left
                right -= 1
            }
        }
    }
    return Z
}

func zSearch(text: String, pattern: String) -> [Int] {
    let str = pattern + "$" + text
    let Z = calculateZArray(string: str)
    var result: [Int] = []
    for i in 0..<Z.count {
        if Z[i] == pattern.count {
            result.append(i - pattern.count - 1)
        }
    }
    return result
}
