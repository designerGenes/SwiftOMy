//
//  RecursionView.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/11/23.
//

import SwiftUI
import MarkdownUI
import Combine

class BinarySearchViewModel: ObservableObject {
    let binaryTree: Tree
    @Published var searchValue: String?
    @Published var foundRanges: [NSRange] = []
        
    init(searchValue: String? = nil) {
        self.searchValue = searchValue
        var arr: [Int] = []
        for x in 0..<50 {
            let val = arr.isEmpty ? Int.random(in: 0..<100) : arr[x - 1] + Int.random(in: 0..<10)
            arr.append(val)
        }
        arr.scramble()
        self.binaryTree = Tree(fromArr: arr)
    }
    
    var searchValueBinding: Binding<String> {
        Binding<String>(get: { self.searchValue ?? "" },
                        set: { self.searchValue = $0 })
    }
}

struct BinarySearchView: View {
    
    @ObservedObject var viewModel: BinarySearchViewModel = BinarySearchViewModel()

    var body: some View {
        ScrollView {
            
            Markdown {
                viewModel.binaryTree.treeStructure().map { val in
                    return String(val) == viewModel.searchValue ? "**\(val)**" : "\(val)"
                }.joined(separator: ", ")
            }
            Divider()
            HStack {
                TextField("Search", text: viewModel.searchValueBinding)
                    .keyboardType(.numberPad)
                    .onReceive(Just(viewModel.searchValue)) { newValue in
                        let filtered = newValue?.filter { "0123456789".contains($0) }
                        if filtered != newValue {
                            viewModel.searchValue = filtered
                        }
                    }
                
                Spacer()
                if let searchValueStr = viewModel.searchValue, let searchValue = Int(searchValueStr) {
                    Text("Found: \(viewModel.binaryTree.search(val: searchValue).0 > 0 ? "Yes" : "No")")
                }
            }
            Divider()
        }
    }
}

struct RecursionView: View {
    @ObservedObject var viewModel: RecursionViewModel = RecursionViewModel()
    
    /**
     This view will contain examples of recursion in the form of solutions to proposed coding challenges.
     The challenges will be:
     1. Implement a function that produces the factorial of a input number
     2. Implement binary search
     3. Implement a solution to the Tower of Hanoi
     4. Demonstrate tree traversal (in-order, pre-order, post-order)
     
     */
    
    var body: some View {
        NavigationView {
            Form {
                Section("Controls") {
                    List(RecursionCase.allValues.indices, id: \.self) { idx in
                        HStack {
                            Text(RecursionCase.allValues[idx].rawValue)
                            Spacer()
                            if viewModel.selectedRecursionCaseIdx == idx {
                                Image(systemName: "checkmark")
                            }
                        }
                        .background {
                            Color.white
                        }
                        .onTapGesture {
                            viewModel.selectedRecursionCaseIdx = idx
                        }
                    }
                }
                Section("Explanation") {
                    Markdown {
                        RecursionCase.allValues[viewModel.selectedRecursionCaseIdx].inChallengeForm()
                    }
                }
                Section("Solution") {
                    if RecursionCase.allValues[viewModel.selectedRecursionCaseIdx] == .binarySearch {
                        BinarySearchView()
                    }
                }
                Section("Code") {
                    
                }
                .padding(EdgeInsets(top: 12, leading: 0, bottom: 0, trailing: 0))
            }
        }
    }
    
}

#Preview {
    RecursionView()
}
