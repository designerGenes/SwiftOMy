import UIKit

func rabinKarpSearch(_ pattern: String, _ text: String) -> [Int] {
    func hash(_ string: [Character]) -> Int {
        var hash = 0
        for char in string {
            hash = (hash * 31 + Int(char.asciiValue!)) % 1000000007
        }
        return hash
    }
    
    func rollingHash(_ hash: Int, _ dropped: Character, _ added: Character, _ patternLength: Int) -> Int {
        let prime = 1000000007
        var droppedEffect = Int(dropped.asciiValue!) * Int(pow(31.0, Double(patternLength - 1))) % prime
        var newHash = (hash - droppedEffect + prime) % prime
        newHash = (newHash * 31 + Int(added.asciiValue!)) % prime
        return newHash
    }

    
    let patternLength = pattern.count
    let textLength = text.count
    guard patternLength > 0 && patternLength <= textLength else { return [] }
    var result: [Int] = []
    let pattern = Array(pattern)
    let text: [Character] = Array(text)
    let patternHash = hash(Array(pattern))
    var textHash = hash(Array(text[0..<patternLength]))
    for i in 0...textLength - patternLength {
        if patternHash == textHash {
            if pattern == Array(text[i..<i + patternLength]) {
                result.append(i)
            }
        }
        if i < textLength - patternLength {
            textHash = rollingHash(textHash, text[i], text[i + patternLength], patternLength)
        }
    }
    return result
}

let text = "Mr. and Mrs. Dursley of number four, Privet Drive, were proud to say that they were perfectly normal, thank you very much. They also had four children, all the age of four. Another boy lived with them named Harry Potter, who was an outlaw but kept around because he had a lot of money in the bank.  Forty-four million British dollars, to be precise, which are known as Rikkety Goldfartiers in England, where they are nearly worthless.  The Dursleys hid an unlicensed daycare center beneath the heating and cooling unit outdoors, for the Dursleys were not in fact normal. The dursleys were actually just profligate liars, and wizards, but I repeat myself."

let pattern = "dursley"

print(rabinKarpSearch(pattern, text))

