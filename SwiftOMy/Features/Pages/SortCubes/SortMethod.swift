//
//  SortMethod.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/9/23.
//

import Foundation
import MarkdownUI
import SwiftUI

enum SortMethod: String {
    case BubbleSort = "Bubble Sort"
    case MergeSort = "Merge Sort"
    case QuickSort = "Quick Sort"
    case ShellSort = "Shell Sort"
    case InsertionSort = "Insertion Sort"
    case SelectionSort = "Selection Sort"
    static let allValues: [SortMethod]  = [.BubbleSort, .MergeSort, .QuickSort, .InsertionSort, .SelectionSort, .ShellSort]
    
    func explanationCode() -> String {
        switch self {
        case .BubbleSort:
            
            return
                """
                    func bubbleSort(_ array: inout [Int]) {
                        for i in 0..<array.count {
                            for j in 1..<array.count - i {
                                if array[j] < array[j - 1] {
                                    array.swapAt(j - 1, j)
                                }
                            }
                        }
                    }
                """
            
        case .InsertionSort:
            
            return
            """
                func insertionSort(_ array: inout [Int]) {
                    for i in 1..<array.count {
                        var j = i
                        while j > 0 && array[j] < array[j - 1] {
                            array.swapAt(j - 1, j)
                            j -= 1
                        }
                    }
                }
            """
            
        case .MergeSort:
            
            return
            """
                func mergeSort(_ array: [Int]) -> [Int] {
                    guard array.count > 1 else { return array }
                    
                    let middleIndex = array.count / 2
                    
                    let leftArray = mergeSort(Array(array[0..<middleIndex]))
                    let rightArray = mergeSort(Array(array[middleIndex..<array.count]))
                    
                    return merge(leftArray, rightArray)
                }
            
                func merge<T: Comparable & Hashable>(l: [T], r: [T], actionQueue: ActionQueue<T>) -> SortingResult<T> {
                    var (lIdx, rIdx) = (0, 0)
                    var out: [T] = []
                    while lIdx < l.count && rIdx < r.count {
                        if l[lIdx] < r[rIdx] {
                            out.append(l[lIdx])
                            lIdx += 1
                        } else {
                            out.append(r[rIdx])
                            rIdx += 1
                        }
                    }
                    out = out + l[lIdx...] + r[rIdx...]
                    actionQueue.actions.append(.merge(l: l, r: r, result: out))
                    return (out, actionQueue)
                }
            """
            
        case .QuickSort:
            
            return
            """
                func quickSort(_ array: inout [Int], low: Int, high: Int) {
                    if low < high {
                        let pivot = partition(&array, low: low, high: high)
                        quickSort(&array, low: low, high: pivot)
                        quickSort(&array, low: pivot + 1, high: high)
                    }
                }
            
            """
            
        case .ShellSort:
            
            return
            """
                func shellSort(_ array: inout [Int]) {
                    var gap = array.count / 2
                    while gap > 0 {
                        for i in gap..<array.count {
                            var j = i
                            while j >= gap && array[j - gap] > array[j] {
                                array.swapAt(j - gap, j)
                                j -= gap
                            }
                        }
                        gap = gap == 2 ? 1 : gap * 5 / 11
                    }
                }
            """
            
        case .SelectionSort:
            
            return
            """
                func selectionSort(_ array: inout [Int]) {
                    for i in 0..<array.count {
                        var minIndex = i
                        for j in i + 1..<array.count {
                            if array[j] < array[minIndex] {
                                minIndex = j
                            }
                        }
                        array.swapAt(i, minIndex)
                    }
                }
            """
            
        }
    }
    
    func explanation() -> String {
        switch self {
        case .BubbleSort:
            return """
            Bubble Sort is the most basic sorting algorithm.  It works by comparing each pair of adjacent elements, and swapping them if they are in the wrong order.  It then repeats this process until the array is sorted.
            """
        case .MergeSort:
            return """
            Merge Sort is a divide-and-conquer algorithm that works by splitting the array into two halves, sorting each half, and then merging the two halves back together.
            """
        case .QuickSort:
            return """
            Quick Sort is a divide-and-conquer algorithm that works by picking an element as a pivot and partitioning the array around the pivot.  It then recursively sorts the two halves of the array.
            
            Quick Sort is often the fastest sorting algorithm, but it can be slow in the worst case.
            
            """
        case .InsertionSort:
            return """
            Insertion Sort is a simple sorting algorithm that works by repeatedly inserting an element from the unsorted portion of the array into the sorted portion of the array.
            """
        case .SelectionSort:
            return """
            Selection Sort is a simple sorting algorithm that works by repeatedly finding the minimum element from the unsorted portion of the array and moving it to the end of the sorted portion of the array.
            """
        case .ShellSort:
            return """
            Shell Sort is a variation of Insertion Sort that works by sorting elements that are far apart from each other and then progressively reducing the gap between elements to be sorted.  It is a relatively efficient algorithm that is faster than Insertion Sort.
            
            """
        }
    }
}
