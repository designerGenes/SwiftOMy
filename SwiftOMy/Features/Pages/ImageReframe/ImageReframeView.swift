//
//  ImageReframeView.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/10/23.
//

import SwiftUI
import UIKit
import CoreGraphics

enum AspectRatio {
    case SixteenNine, NineSixteen, OneOne, ThreeFour, FourThree
    func numericRatio() -> (CGFloat, CGFloat) {
        switch self {
        case .SixteenNine:
            return (16, 9)
        case .FourThree:
            return (4, 3)
        case .NineSixteen:
            return (9, 16)
        case .ThreeFour:
            return (3, 4)
        case .OneOne:
            return (1, 1)
        }
    }
}

struct ImageReframeView: View {
    @ObservedObject var viewModel: ImageReframeViewModel = ImageReframeViewModel()
    
    
    var body: some View {
        NavigationView {
            VStack(spacing: 16) {
                ZStack {
                    Rectangle()
                        .foregroundColor(.clear)
                        .frame(height: UIScreen.main.bounds.height * 0.6)
                        .background {
                            LinearGradient(gradient: Gradient(colors: [Color.purple, Color.blue]), startPoint: .top, endPoint: .bottom)
                        }
                        .cornerRadius(6)
                    VStack {
                        if let loadedSizedImage = viewModel.loadedImage?.resizeToAspectRatio(aspectRatio: viewModel.loadedAspectRatio) {
                            Image(uiImage: loadedSizedImage)
                                
                                .scaledToFit()
                                
                                .cornerRadius(6)
                                .border(Color.gray, width: 1)
                        } else {
                            Button(action: {
                                viewModel.selectImage()
                            }, label: {
                                Text("Select Image")
                                    .font(.system(.title, design: .rounded))
                                    .fontWeight(.bold)
                                    .foregroundColor(.white)
                                    .padding()
                            })
                        }
                    }
                    
                }
                HStack(spacing: 32) {
                    Button(action: {
                        viewModel.loadedAspectRatio = .SixteenNine
                    }, label: {
                        Text("16:9")
                    })
                    .foregroundColor(viewModel.loadedAspectRatio == .SixteenNine ? .black : .blue)
                    
                    Button(action: {
                        viewModel.loadedAspectRatio = .FourThree
                    }, label: {
                        Text("4:3")
                    })
                    .foregroundColor(viewModel.loadedAspectRatio == .FourThree ? .black : .blue)
                    
                    Button(action: {
                        viewModel.loadedAspectRatio = .OneOne
                    }, label: {
                        Text("1:1")
                    })
                    .foregroundColor(viewModel.loadedAspectRatio == .OneOne ? .black : .blue)
                    
                    Button(action: {
                        viewModel.loadedAspectRatio = .ThreeFour
                    }, label: {
                        Text("3:4")
                    })
                    .foregroundColor(viewModel.loadedAspectRatio == .ThreeFour ? .black : .blue)
                    
                    
                    Button(action: {
                        viewModel.loadedAspectRatio = .NineSixteen
                    }, label: {
                        Text("9:16")
                    })
                    .foregroundColor(viewModel.loadedAspectRatio == .NineSixteen ? .black : .blue)
                    
                    Button(action: {
                        viewModel.loadedAspectRatio = .NineSixteen
                    }, label: {
                        Text("↩️")
                    })
                    .foregroundColor(viewModel.loadedAspectRatio == .NineSixteen ? .black : .blue)
                }
                .padding()
                .background {
                    RoundedRectangle(cornerRadius: 6)
                        .fill(Color.gray.opacity(0.4))
                }
                
                
            }
            .padding()
        }
    }
}

#Preview {
    ImageReframeView()
}
