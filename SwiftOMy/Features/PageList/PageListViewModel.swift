//
//  PageListViewModel.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/5/23.
//

import Foundation
import SwiftUI

class PageListViewModel: ObservableObject {
    var appName: String {
        Bundle.main.infoDictionary?["CFBundleName"] as! String
    }
    var appExplanation: String {
        return "\(appName) serves as a dynamic and interactive guide for individuals aiming to comprehend the functioning of complex algorithms. Users can customize various parameters like the size of data sets or patterns to search for in custom blocks of text, enabling them to see how these algorithms scale and perform under different conditions."
    }
    
    let pages: [SwiftOhMyPage] = [
        SwiftOhMyPage(title: "Cube Sorting", pageData: AnyView(
            SortCubesPage()
        )),
        SwiftOhMyPage(title: "Text Searching", pageData: AnyView (
            SearchPatternsView()
        )),
        SwiftOhMyPage(title: "Recursion", pageData: AnyView(
           RecursionView()
        ))
    ]
}
