//
//  Array+Random.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/6/23.
//

import Foundation

extension Array {
    mutating func scramble() {
        for x in 0..<self.count {
            self.swapAt(x, Int.random(in: 0..<self.count))
        }
    }
}

extension Array where Element: Comparable {
    func isSorted() -> Bool {
        guard self.count > 1 else {
            return !self.isEmpty
        }
        for x in 1..<self.count {
            if self[x - 1] >= self[x] {
                return false
            }
        }
        return true
    }
}
