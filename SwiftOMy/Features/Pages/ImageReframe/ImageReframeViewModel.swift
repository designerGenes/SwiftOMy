//
//  ImageReframeViewModel.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/10/23.
//

import Foundation
import SwiftUI
import UIKit
import CoreGraphics
import CoreImage

class ImageReframeViewModel: NSObject, ObservableObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @Published var loadedImage: UIImage?
    @Published var loadedAspectRatio: AspectRatio = .SixteenNine
    
    // UIImagePickerControllerDelegate methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = (info[.editedImage] ?? info[.originalImage]) as? UIImage {
            loadedImage = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
        
    func selectImage() {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        picker.mediaTypes = ["public.image"]
        UIApplication.shared.windows.first?.rootViewController?.present(picker, animated: true, completion: nil)
    }
}

// function to resize image to fit within frame with aspect ratio of x:y where x is width and y is height
// https://stackoverflow.com/questions/31314412/how-to-resize-image-in-swift

enum ResizingProspect {
    case widthSmaller_heightSmaller, widthSmaller_heightLarger, widthLarger_heightSmaller, widthLarger_heightLarger
}

extension UIImage {
    func resizeToAspectRatio(aspectRatio: AspectRatio) -> UIImage? {
        let (width, height) = aspectRatio.numericRatio()
        return self.resizeToAspectRatio(width: width, height: height)
    }
    
    func resizeToAspectRatio(width: CGFloat, height: CGFloat) -> UIImage? {
        // compute size of prospective frame if aspect ratio width:height was applied to self
        let prospectiveSize = CGSize(width: self.size.width * width / height, height: self.size.height * height / width)
        var resizingProspect: ResizingProspect
        if prospectiveSize.width < self.size.width {
            if prospectiveSize.height < self.size.height {
                resizingProspect = .widthSmaller_heightSmaller
            } else {
                resizingProspect = .widthSmaller_heightLarger
            }
        } else {
            if prospectiveSize.height < self.size.height {
                resizingProspect = .widthLarger_heightSmaller
            } else {
                resizingProspect = .widthLarger_heightLarger
            }
        }
        
        /** crop image to match width and height parameters */
        var croppedRect = CGRect.zero
        switch resizingProspect {
        case .widthSmaller_heightSmaller:
            croppedRect.size.width = prospectiveSize.width
            croppedRect.size.height = self.size.height
            croppedRect.origin.x = (self.size.width - prospectiveSize.width) / 2.0
            croppedRect.origin.y = 0
        case .widthSmaller_heightLarger:
            croppedRect.size.width = self.size.width
            croppedRect.size.height = prospectiveSize.height
            croppedRect.origin.x = 0
            croppedRect.origin.y = (self.size.height - prospectiveSize.height) / 2.0
        case .widthLarger_heightLarger:
            croppedRect.size.width = prospectiveSize.width
            croppedRect.size.height = self.size.height
            croppedRect.origin.x = (self.size.width - prospectiveSize.width) / 2.0
            croppedRect.origin.y = 0
        case .widthLarger_heightSmaller:
            croppedRect.size.width = self.size.width
            croppedRect.size.height = prospectiveSize.height
            croppedRect.origin.x = 0
            croppedRect.origin.y = (self.size.height - prospectiveSize.height) / 2.0
        }
        
       
        
        let aspectWidth = size.width / width
        let aspectHeight = size.height / height
        let aspectRatio = max(aspectWidth, aspectHeight)
        
        var scaledImageRect = CGRect.zero
        

        scaledImageRect.size.width = size.width / aspectRatio
        scaledImageRect.size.height = size.height / aspectRatio
        scaledImageRect.origin.x = (width - size.width / aspectRatio) / 2.0
        scaledImageRect.origin.y = (height - size.height / aspectRatio) / 2.0
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
        draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
//        
        return scaledImage
//        case .widthSmaller_heightSmaller:
//            <#code#>
//        case .widthSmaller_heightLarger:
//            <#code#>
//        case .widthLarger_heightSmaller:
//            <#code#>
//        case .widthLarger_heightLarger:
//            <#code#>
        }
}
