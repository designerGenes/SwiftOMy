//
//  SearchPatternsView.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/11/23.
//

import SwiftUI
import UIKit
import MarkdownUI

struct HighlightingTextView: UIViewRepresentable {
    @Binding var text: String
    @Binding var highlightedRanges: [NSRange] // Array of NSRange representing the ranges to be highlighted
    
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.isEditable = true
        textView.isSelectable = true
        textView.font = UIFont.preferredFont(forTextStyle: .body).withSize(16)
        textView.delegate = context.coordinator
        textView.backgroundColor = UIColor.white
        textView.textColor = .black
        return textView
    }
    
    func updateUIView(_ uiView: UITextView, context: Context) {
        DispatchQueue.main.async {
            // Remember the current cursor position
            let range = uiView.selectedTextRange
            
            // Update the text
            uiView.text = text
            highlightRanges(in: uiView)

            // Restore the cursor position
            uiView.selectedTextRange = range
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    private func highlightRanges(in textView: UITextView) {
        let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(.backgroundColor, value: UIColor.clear, range: NSRange(location: 0, length: attributedText.length))

            for range in highlightedRanges {
                if NSMaxRange(range) <= attributedText.length {
                    attributedText.addAttribute(.backgroundColor, value: UIColor.yellow, range: range)
                } else {
                    print("Attempted to highlight a range that was out of bounds: \(range)")
                }
            }

            textView.attributedText = attributedText
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        let parent: HighlightingTextView
        
        init(_ parent: HighlightingTextView) {
            self.parent = parent
        }
        
        func textViewDidChange(_ textView: UITextView) {
            parent.text = textView.text
        }
    }
}


struct SearchPatternsView: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel: SearchPatternsViewModel = SearchPatternsViewModel()
    
    var body: some View {
        NavigationView {
            Form {
                Section("Input") {
                    VStack(spacing: 12) {
                        Spacer()
                            .frame(height: 8)
                        HStack {
                            TextField("Pattern", text: $viewModel.inputPattern)
                                .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                                .background(Color.white.opacity(0.1))
                                .cornerRadius(6)
                            
                            Button(action: {
                                
                            }, label: {
                                Text("Random")
                                    .foregroundColor(colorScheme == .light ? Color.blue : .white)
                            })
                            Spacer()
                        }
                        Divider()
                        
                        HStack {
                            Spacer()
                            HighlightingTextView(text: $viewModel.inputString, highlightedRanges: $viewModel.patternDiscoveredRanges)
                                .frame(minHeight: 200)
                            
                                .cornerRadius(6)
                            
                            Spacer()
                        }
                        Divider()
                        HStack {
                            Text("The pattern was found \(viewModel.patternDiscoveredRanges.count) times")
                                .font(.subheadline)
                            Spacer()
                        }
                        Spacer()
                            .frame(height: 4)
                    }
                }
                Section("Tuning") {
                    VStack(spacing: 12) {
                        List(SearchAlgorithm.allCases.indices, id: \.self) { algorithmIdx in
//                            VStack(spacing: 0) {
                                HStack {
                                    Text(SearchAlgorithm.allCases[algorithmIdx].rawValue)
                                        .font(.footnote)
                                    Spacer()
                                    if viewModel.selectedSearchMethodIdx == algorithmIdx {
                                        // checkmark image
                                        Image(systemName: "checkmark")
                                        
                                    }
                                }
                                .padding(EdgeInsets(top: 4, leading: 0, bottom: 4, trailing: 0))
                                .background {
                                    Color.white
                                }
                                .onTapGesture {
                                    withAnimation {
                                        viewModel.selectedSearchMethodIdx = algorithmIdx
                                        viewModel.selectedSearchMethodIsExpanded = !viewModel.selectedSearchMethodIsExpanded
                                    }
                                }
//                            }
                            
                        }
                    }
                }
                Section("Explanation") {
                    VStack {
                        Text("\(viewModel.searchMethod.explanation())")
                            .font(.footnote)
                        Divider()
                        Markdown {
                            viewModel.searchMethod.explanationCode()
                        }
                        .fontWidth(.compressed)
                        .markdownTheme(.gitHub)
                        
                    }
                        
                }
            }
            .navigationTitle("Search Algorithms")
        }
    }
}
#Preview {
    SearchPatternsView()
}
