//
//  SearchPatternsViewModel.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/11/23.
//

import Foundation
import SwiftUI
import Combine



class SearchPatternsViewModel: ObservableObject {
    var cancellables = Set<AnyCancellable>()
    
    @Published var inputString: String = "Mr. and Mrs. Dursley of number four, Privet Drive, were proud to say that they were perfectly normal, thank you very much. They also had four children, all the age of four. Another boy lived with them named Harry Potter, who was an outlaw but kept around because he had a lot of money in the bank.  Forty-four million British dollars, to be precise, which are known as Rikkety Goldfartiers in England, where they are nearly worthless.  The Dursleys hid an unlicensed daycare center beneath the heating and cooling unit outdoors, for the Dursleys were not in fact normal. The Dursleys were actually just profligate liars, and wizards, but I repeat myself."
    @Published var inputPattern: String = "four"
    @Published var patternDiscoveredRanges: [NSRange] = []
    @Published var selectedSearchMethodIdx: Int = 0
    @Published var selectedSearchMethodIsExpanded: Bool = false
    var searchMethod: SearchAlgorithm {
        return SearchAlgorithm.allCases[selectedSearchMethodIdx]
    }
        
    func searchUsing(_ searchMethod: SearchAlgorithm) -> [Int] {
        switch searchMethod {
        case .rabinKarp:
            return rabinKarpSearch(inputPattern, inputString)
        case .knuthMorrisPratt:
            return knuthMorrisPrattSearch(inputPattern, inputString)
        case .boyerMoore:
            return boyerMooreSearch(inputPattern, inputString)
        case .Z:
            return zSearch(text: inputString, pattern: inputPattern)
        }
    }
    
    init() {
        $inputPattern
            .combineLatest($inputString, $selectedSearchMethodIdx)
            .map { (pattern, text, searchMethodIdx) -> ([NSRange], String) in
                let startIdxs = self.searchUsing(SearchAlgorithm.allCases[searchMethodIdx])
                let ranges = startIdxs.map { NSRange(location: $0, length: pattern.count) }
                return (ranges, text)
            }
            .sink { [weak self] ranges, text in
                self?.patternDiscoveredRanges = ranges
                self?.inputString = text
            }
            .store(in: &cancellables)
    }
}





