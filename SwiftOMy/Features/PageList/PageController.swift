//
//  PageController.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/4/23.
//

import Foundation
import SwiftUI

struct SwiftOhMyPage: Identifiable {
    var title: String
    var id: String = UUID().uuidString
    var pageData: AnyView
}

struct GlowBorderPage: View {
    var body: some View {
        VStack {
            Text("Glow Border")
            Spacer()
            HStack {
                Rectangle()
                    .frame(width: 100, height: 100)
                    .border(Color.black)
            }
            
        }
    }
}


