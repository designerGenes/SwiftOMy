//
//  SortCubesPage.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/6/23.
//

import SwiftUI
import MarkdownUI

struct SortCubesPage: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel: SortCubesViewModel = SortCubesViewModel()
    
    
    
    var selectedSortMethod: SortMethod {
        SortMethod.allValues[viewModel.selectedSortMethodIdx]
    }
    
    var ControlSection: some View {
        Section("Controls section") {
            VStack(spacing: 16) {
                HStack {
                    Slider(value: $viewModel.numberOfCubes, in: 2...40)
                        .accentColor(Color.purple.opacity(0.5 + (0.5 * (viewModel.numberOfCubes / 40))))
                        .onChange(of: viewModel.numberOfCubes) { (_, _) in
                            viewModel.cubeVals = viewModel.generateCubes()
                        }
                    
                    Text("\(Int(viewModel.numberOfCubes)) cubes")
                }
                HStack {
                    Text(self.selectedSortMethod.rawValue)
                    
                    Stepper("", value: $viewModel.selectedSortMethodIdx, in: 0...SortMethod.allValues.count-1)
                        .onChange(of: viewModel.selectedSortMethodIdx) { (_, _) in
                            viewModel.cubeVals = viewModel.generateCubes()
                        }
                    
                }
                HStack {
                    Button(viewModel.showExplanation ? "Hide" : "Explain") {
                        viewModel.showExplanation = !viewModel.showExplanation
                    }
                    .buttonStyle(BorderlessButtonStyle())
                    .padding()
                    .foregroundColor(.white)
                    .frame(width: 120, height: 48)
                    .background {
                        ContainerRelativeShape()
                        // gradient fill from purple to blue
                            .fill(LinearGradient(gradient: Gradient(colors: [Color.purple, Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
                            .cornerRadius(6)
                        
                    }
                    Spacer()
                        .frame(width: 16)
                    
                    Button("Sort") {
                        viewModel.sortCubes()
                    }
                    .buttonStyle(BorderlessButtonStyle())
                    .foregroundColor(.white)
                    .frame(width: 120, height: 48)
                    .background {
                        ContainerRelativeShape()
                            .fill(LinearGradient(gradient: Gradient(colors: [Color.purple, Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
                            .cornerRadius(6)
                    }
                    .disabled(viewModel.cubeVals.isSorted())
                    Spacer()
                }
            }
            
            
        }
    }
    
    var ExplanationSection: some View {
        Section("Explanation section") {
            VStack {
                Text(selectedSortMethod.explanation())
                    .font(.body)
                    .padding()
                Markdown {
                    selectedSortMethod.explanationCode()
                }
                .markdownTheme(.gitHub)
            }
        }
    }
    
    var CubesSection: some View {
        Section("\(viewModel.cubeVals.isSorted() ? "Sorted" : "Unsorted") cubes") {
            ScrollView(.horizontal) {
                HStack {
                    ForEach(viewModel.cubeVals, id: \.self) { cubeVal in
                        ZStack {
                            Rectangle()
                                .frame(height: 40)
                                .foregroundColor(.clear)
                            VStack {
                                CubeView(digit: cubeVal)
                                    .frame(width: 64, height: 64)
                                    .shadow(radius: 6, x: 2, y: 4)
                                Spacer()
                            }
                        }
                    }
                }
            }
        }
    }
    
    var StepsSection: some View {
        Section("\(viewModel.actionQueue.actions.count) steps") {
            LazyVStack {
                ScrollView {
                    ForEach(viewModel.actionQueue.actions.indices, id: \.self) { idx in
                        HStack {
                            Text("\(idx + 1).")
                                .font(.footnote)
                            Spacer()
                                .frame(width: 6)
                            Text(viewModel.actionQueue.actions[idx].described())
                                .font(.footnote)
                            Spacer()
                        }
                        .cornerRadius(6)
                        .padding()
                        
                    }
                }
            }
            
        }
    }
    
    var body: some View {
        NavigationView {
            List {
                ControlSection
                if viewModel.showExplanation {
                    ExplanationSection
                }
                if !viewModel.cubeVals.isEmpty {
                    CubesSection
                }
                if !viewModel.actionQueue.actions.isEmpty && viewModel.cubeVals.isSorted() {
                    StepsSection
                }
            }
            .listStyle(GroupedListStyle())
            .environment(\.defaultMinListRowHeight, 44)
            .animation(Animation.easeInOut(duration: 0.5))
            .navigationTitle("Cube sorting")
        }
        
    }
}

#Preview {
    SortCubesPage()
}
