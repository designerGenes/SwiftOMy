//
//  SwiftOMyApp.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/3/23.
//

import SwiftUI

@main
struct SwiftOMyApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            PageListView(viewModel: PageListViewModel())
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
