//
//  BinarySearch.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/13/23.
//

import Foundation

final class Tree {
    var root: Node
    init(root: Node) {
        self.root = root
    }
    var allChildren: [Node] {
        let left: [Node] = root.leftChildren
        let right: [Node] = root.rightChildren
        return left + right
    }
    
    func treeStructure() -> [Int] {
        let nodes = [self.root] + self.root.leftChildren + self.root.rightChildren
        return nodes.map { $0.value }
        
//        var output = ""
//        var currentLevel: [Node] = [root]
//        var nextLevel: [Node] = []
//        var level = 0
//        while !currentLevel.isEmpty {
//            output += "Level \(level): "
//            for node in currentLevel {
//                output += "\(node.value) "
//                if let left = node.left {
//                    nextLevel.append(left)
//                }
//                if let right = node.right {
//                    nextLevel.append(right)
//                }
//            }
//            output += "\n"
//            currentLevel = nextLevel
//        }
//        return output
    }
    
    init(fromArr arr: [Int]) {
        self.root = Node(value: arr[0])
        for i in 1..<arr.count {
            insertNode(arr[i])
        }
    }
    
    func search(val: Int, steps: Int = 0, currentNode: Node? = nil, recursionFrames: [RecursionFrame] = []) -> (Int, [RecursionFrame]) {
        var recursionFrames = recursionFrames + [
            .calledSelf(fnName: "binary search", inputVals: ["searchNode": currentNode?.value])
        ]
        let currentNode = currentNode ?? self.root
        var steps = steps + 1
        if val == currentNode.value {
            return (steps, recursionFrames + [.foundValue(value: val)])
        } else if val < currentNode.value {
            if let left = currentNode.left {
                return search(val: val, steps: steps, currentNode: left, recursionFrames: recursionFrames)
            } else {
                return (-1, recursionFrames + [.foundNothing])
            }
        } else {
            if let right = currentNode.right {
                return search(val: val, steps: steps, currentNode: right, recursionFrames: recursionFrames)
            } else {
                return (-1, recursionFrames + [.foundNothing])
            }
        }
        
        
    }
    
    func insertNode(_ value: Int) {
        let newNode = Node(value: value)
        var currentNode = self.root
        while true {
            if value < currentNode.value {
                if let left = currentNode.left {
                    currentNode = left
                } else {
                    currentNode.left = newNode
                    break
                }
            } else {
                if let right = currentNode.right {
                    currentNode = right
                } else {
                    currentNode.right = newNode
                    break
                }
            }
        }
    }
}

func convertArrayToTree(_ arr: [Int]) -> Tree {
    let root = Node(value: arr[0])
    var tree = Tree(root: root)
    for i in 1..<arr.count {
        tree.insertNode(arr[i])
    }
    return tree
}



final class Node {
    let value: Int
    var left: Node?
    var right: Node?
    init(value: Int, left: Node? = nil, right: Node? = nil) {
        self.value = value
        self.left = left
        self.right = right
    }
    
    // get all child nodes from left hand side
    var leftChildren: [Node] {
        var children: [Node] = []
        if let left = left {
            children.append(left)
            children.append(contentsOf: left.leftChildren)
            children.append(contentsOf: left.rightChildren)
        }
        return children
    }
    
    var rightChildren: [Node] {
        var children: [Node] = []
        if let right = right {
            children.append(right)
            children.append(contentsOf: right.leftChildren)
            children.append(contentsOf: right.rightChildren)
        }
        return children
    }
}
