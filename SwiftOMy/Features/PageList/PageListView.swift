//
//  ContentView.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/3/23.
//

import SwiftUI
import CoreData

struct PageListView: View {
    @ObservedObject var viewModel: PageListViewModel = PageListViewModel()
    
    var body: some View {
        NavigationStack{
            Form {
                Section("Demonstrations") {
                    List(viewModel.pages) {  page in
                        NavigationLink(destination: page.pageData) {
                            HStack {
                                Text(page.title)
                                Spacer()
                            }
                        }
                    }
                }
                Section("Explanation") {
                    Text(viewModel.appExplanation)
                }
            }
        }
    }
    
}


#Preview {
    PageListView(viewModel: PageListViewModel())
}
