//
//  CubeView.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/7/23.
//

import SwiftUI

struct CubeView: View {
    var digit: Int = 0
    var body: some View {
        Text("\(digit)")
            .font(.title)
            .fontWeight(.bold)
            .foregroundColor(.white)
            .background {
                ContainerRelativeShape()
                // color with gradient from purple to blue
                    .fill(LinearGradient(gradient: Gradient(colors: [Color.purple, Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
//                    .foregroundColor(Gradient(colors: [Color.purple, Color.blue]))
                    .border(Color.purple.opacity(0.2), width: 3)
                    .cornerRadius(8)
                    .frame(minWidth: 40, minHeight: 40)
                    .shadow(color: Color.black.opacity(0.2), radius: 2, x: 2, y: 2)
                
            }
    }
}

#Preview {
    CubeView()
}
