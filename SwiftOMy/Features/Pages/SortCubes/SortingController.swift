//
//  Array+Sort.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/6/23.
//

import Foundation
import UIKit

typealias SortingResult<T: Comparable & Hashable> = ([T], ActionQueue<T>)

enum SortingAction<T: Comparable & Hashable>: Hashable {
    case swap(l: T, r: T)
    case divide(arr: [T], splitIdx: Int)
    case merge(l: [T], r: [T], result: [T])
    case selectPivot(pivot: T, arr: [T])
    case select(arr: [T], selectedIdx: Int)
    case insert(arr: [T], insertedIdx: Int)
    case count(arr: [T], countedIdx: Int)
    
    func described() -> String {
        switch self {
        case .swap(l: let l, r: let r):
            return "Swapped \(l) and \(r)"
        case .divide(arr: let arr, splitIdx: let splitIdx):
            return "Divided array at \(splitIdx) into \(arr[0..<splitIdx]) and \(arr[splitIdx...])"
        case .merge(l: let l, r: let r, result: let result):
            return "Merged \(l) and \(r) into \(result)"
        case .selectPivot(pivot: let pivot, arr: let arr):
            return "Selected \(pivot) as pivot for \(arr)"
        case .select(arr: let arr, selectedIdx: let selectedIdx):
            return "Selected \(arr[selectedIdx]) from \(arr)"
        case .insert(arr: let arr, insertedIdx: let insertedIdx):
            return "Inserted \(arr[insertedIdx]) into \(arr)"
        case .count(arr: let arr, countedIdx: let countedIdx):
            return "Counted \(arr[countedIdx]) in \(arr)"
        
        }
        
    }
    func hash(into hasher: inout Hasher) {
            switch self {
            case .swap(let l, let r):
                hasher.combine("swap")
                hasher.combine(l)
                hasher.combine(r)
            case .divide(let arr, let splitIdx):
                hasher.combine("divide")
                hasher.combine(arr)
                hasher.combine(splitIdx)
            case .merge(let l, let r, let result):
                hasher.combine("merge")
                hasher.combine(l)
                hasher.combine(r)
                hasher.combine(result)
            case .selectPivot(let pivot, let arr):
                hasher.combine("selectPivot")
                hasher.combine(pivot)
                hasher.combine(arr)
            case .select(let arr, let selectedIdx):
                hasher.combine("select")
                hasher.combine(arr)
                hasher.combine(selectedIdx)
            case .insert(let arr, let insertedIdx):
                hasher.combine("insert")
                hasher.combine(arr)
                hasher.combine(insertedIdx)
            case .count(let arr, let countedIdx):
                hasher.combine("count")
                hasher.combine(arr)
                hasher.combine(countedIdx)
            
            }
        }
        
        static func == (lhs: SortingAction<T>, rhs: SortingAction<T>) -> Bool {
            switch (lhs, rhs) {
            case (.swap(let lhsL, let lhsR), .swap(let rhsL, let rhsR)):
                return lhsL == rhsL && lhsR == rhsR
            case (.divide(let lhsArr, let lhsIdx), .divide(let rhsArr, let rhsIdx)):
                return lhsArr == rhsArr && lhsIdx == rhsIdx
            case (.merge(let lhsL, let lhsR, let lhsResult), .merge(let rhsL, let rhsR, let rhsResult)):
                return lhsL == rhsL && lhsR == rhsR && lhsResult == rhsResult
            case (.selectPivot(let lhsPivot, let lhsArr), .selectPivot(let rhsPivot, let rhsArr)):
                return lhsPivot == rhsPivot && lhsArr == rhsArr
            default:
                return false
            }
        }
}

class ActionQueue<T: Comparable & Hashable> {
    var actions: [SortingAction<T>] = []
}


func bubbleSort<T: Comparable & Hashable>(arr: inout [T]) -> SortingResult<T> {
    let actionQueue = ActionQueue<T>()
    for x in 0..<arr.count {
        for y in x+1..<arr.count {
            if arr[x] > arr[y] {
                arr.swapAt(x, y)
                actionQueue.actions.append(SortingAction.swap(l: arr[x], r: arr[y]))
            }
        }
    }
    return (arr, actionQueue)
}

func mergeSort<T: Comparable & Hashable>(arr: inout [T], actionQueue: ActionQueue<T> = ActionQueue()) -> SortingResult<T> {
    guard arr.count > 1 else {
        return (arr, actionQueue)
    }
    let midIdx = arr.count / 2
    var l = Array(arr[0..<midIdx])
    var r = Array(arr[midIdx...])
    actionQueue.actions.append(.divide(arr: arr, splitIdx: midIdx))
    let leftMergeSort = mergeSort(arr: &l, actionQueue: actionQueue)
    let rightMergeSort = mergeSort(arr: &r, actionQueue: actionQueue)
    let out = merge(l: leftMergeSort.0, r: rightMergeSort.0, actionQueue: actionQueue)
    
    return (out.0, actionQueue)
}

func merge<T: Comparable & Hashable>(l: [T], r: [T], actionQueue: ActionQueue<T>) -> SortingResult<T> {
    var (lIdx, rIdx) = (0, 0)
    var out: [T] = []
    while lIdx < l.count && rIdx < r.count {
        if l[lIdx] < r[rIdx] {
            out.append(l[lIdx])
            lIdx += 1
        } else {
            out.append(r[rIdx])
            rIdx += 1
        }
    }
    out = out + l[lIdx...] + r[rIdx...]
    actionQueue.actions.append(.merge(l: l, r: r, result: out))
    return (out, actionQueue)
}

func quickSort<T: Comparable & Hashable>(arr: [T], actionQueue: ActionQueue<T> = ActionQueue()) -> SortingResult<T> {
    guard arr.count > 1 else {
        return (arr, actionQueue)
    }
    let pivot = arr[arr.count / 2]
    actionQueue.actions.append(.selectPivot(pivot: pivot, arr: arr))
    let less = arr.filter { $0 < pivot }
    let equal = arr.filter { $0 == pivot }
    let more = arr.filter { $0 > pivot }
    let leftQuickSort = quickSort(arr: less, actionQueue: actionQueue)
    let rightQuickSort = quickSort(arr: more, actionQueue: actionQueue)
    let result = leftQuickSort.0 + equal + rightQuickSort.0
    actionQueue.actions.append(.merge(l: leftQuickSort.0, r: rightQuickSort.0, result: result))
    return (result, actionQueue)
}

func selectionSort<T: Comparable & Hashable>(arr: inout [T]) -> SortingResult<T> {
    let actionQueue = ActionQueue<T>()
    for i in 0..<arr.count {
        var minIndex = i
        for j in i+1..<arr.count {
            if arr[j] < arr[minIndex] {
                minIndex = j
            }
        }
        if minIndex != i {
            arr.swapAt(i, minIndex)
            actionQueue.actions.append(.swap(l: arr[i], r: arr[minIndex]))
        }
    }
    return (arr, actionQueue)
}

func insertionSort<T: Comparable & Hashable>(arr: inout [T]) -> SortingResult<T> {
    let actionQueue = ActionQueue<T>()
    for i in 1..<arr.count {
        var j = i
        while j > 0 && arr[j] < arr[j-1] {
            arr.swapAt(j, j-1)
            actionQueue.actions.append(.swap(l: arr[j], r: arr[j-1]))
            j -= 1
        }
    }
    return (arr, actionQueue)
}

func shellSort<T: Comparable & Hashable>(arr: inout [T]) -> SortingResult<T> {
    let actionQueue = ActionQueue<T>()
    var gap = arr.count / 2
    while gap > 0 {
        for i in gap..<arr.count {
            var j = i
            while j >= gap && arr[j] < arr[j - gap] {
                arr.swapAt(j, j - gap)
                actionQueue.actions.append(.swap(l: arr[j], r: arr[j - gap]))
                j -= gap
            }
        }
        gap = gap / 2
    }
    return (arr, actionQueue)
}
