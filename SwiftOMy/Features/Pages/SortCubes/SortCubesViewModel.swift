//
//  SortCubesViewModel.swift
//  SwiftOMy
//
//  Created by Jaden Nation on 7/6/23.
//

import Foundation
import SwiftUI



final class SortCubesViewModel: ObservableObject {
    @Published var numberOfCubes: Double = 5 {
        didSet {
            actionQueue.actions.removeAll()
        }
    }
    @Published var selectedSortMethodIdx: Int = 0
    @Published var cubeVals: [Int] = Array(0..<5).shuffled()
    
    @Published var actionQueue: ActionQueue<Int> = ActionQueue()
    @Published var showExplanation: Bool = false
    
    
    
    
    func codeBlockText(txt: LocalizedStringKey) -> some View {
        return ZStack {
            Text(txt)
                .font(.system(.body, design: .monospaced))
                .padding()
                .background {
                    RoundedRectangle(cornerRadius: 8)
                        .fill(Color.white.opacity(0.4))
                }
            ContainerRelativeShape()
                .foregroundColor(Color.gray.opacity(0.4))
        }
    }
    
    func generateCubes() -> [Int] {
        var cVals: [Int] = []
        for _ in 0..<Int(numberOfCubes) {
            let cubeVal = cVals.isEmpty ? 0 : cVals[cVals.count - 1] + Int.random(in: 1..<10)
            cVals.append(cubeVal)
        }
        cVals.scramble()
        return cVals
    }
    
    func sortCubes() {
        switch SortMethod.allValues[selectedSortMethodIdx] {
        case .BubbleSort:
            (cubeVals, actionQueue) = bubbleSort(arr: &cubeVals)
        case .MergeSort:
            (cubeVals, actionQueue) = mergeSort(arr: &cubeVals)
        case .QuickSort:
            (cubeVals, actionQueue) = quickSort(arr: cubeVals)
        case .InsertionSort:
            (cubeVals, actionQueue) = insertionSort(arr: &cubeVals)
        case .SelectionSort:
            (cubeVals, actionQueue) = selectionSort(arr: &cubeVals)
        case .ShellSort:
            (cubeVals, actionQueue) = shellSort(arr: &cubeVals)
        }
    }
    
}
